package main

import (
	"encoding/csv"
	"fmt"
	"os"
	"sort"
)

// value for each letter in the US alphabet
var ALPHABETICAL_VALUE = map[string]int{
	"A": 1,
	"B": 2,
	"C": 3,
	"D": 4,
	"E": 5,
	"F": 6,
	"G": 7,
	"H": 8,
	"I": 9,
	"J": 10,
	"K": 11,
	"L": 12,
	"M": 13,
	"N": 14,
	"O": 15,
	"P": 16,
	"Q": 17,
	"R": 18,
	"S": 19,
	"T": 20,
	"U": 21,
	"V": 22,
	"W": 23,
	"X": 24,
	"Y": 25,
	"Z": 26,
}

func main() {
	// open name file
	f, err := os.Open("./p022_names.txt")
	if err != nil {
		fmt.Println("error:", err.Error())
	}

	csvReader := csv.NewReader(f)
	// get records from csv file
	record, err := csvReader.Read()
	if err != nil {
		fmt.Println("error:", err.Error())
	}

	// sort the names (increasing order)
	sort.Strings(record)

	var sum uint64
	// iterate over names and sum the product of theier index and alphabetical value
	for index, val := range record {
		sum += uint64(index+1) * uint64(getAlphabeticalValue(val))
	}
	fmt.Println("Sum", sum)
}

func getAlphabeticalValue(name string) int {
	var sum int
	for _, letter := range name {
		sum += ALPHABETICAL_VALUE[string(letter)]
	}
	return sum
}
