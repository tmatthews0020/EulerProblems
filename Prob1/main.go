package main // Euler Problem 1
import "fmt"

//
// If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
//
// Find the sum of all the multiples of 3 or 5 below 1000.

// looking for all natural numbers less than this number that are multiples of three or five
const MAX_NUMBER int = 1000

func main() {
	// hold the sum
	var sum int
	// iterate until MAX_NUMBER is reached
	for n := 1; n < MAX_NUMBER; n++ {
		// check if multiple of three or 5
		if n%3 == 0 || n%5 == 0 {
			sum += n
		}
	}
	fmt.Println("Sum", sum)
}
