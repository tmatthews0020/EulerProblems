package main

import (
	"fmt"
	"math"
	"time"
)

// all about that 0 bit storage
var abundantNumbers = make(map[uint64]struct{})

// god im really doing this
var copyAbundantNumbers = make(map[uint64]struct{})

// ehhh
var sumOfAllAbundantNumbers = make(map[uint64]struct{})

// all numbers above this can be written as the sum of two abundant numbers
const upperLimit uint64 = 28123

func main() {
	t := time.Now()
	getAbundantNumbers()
	tAfter := time.Since(t)
	fmt.Println(tAfter)
	// last run took about 18 seconds, not good, probably could do alot of this better with some
	// cool math hacks and less type casts is always good
}

func getAbundantNumbers() {
	// until upperLimit find all the abundant numbers
	for i := uint64(2); i <= upperLimit; i++ {
		if isAbundantNumber(i) {
			abundantNumbers[i] = struct{}{}
		}
	}
	// copy abundantNumbers so we can loop over them both and get all the possible sums
	copyAbundantNumbers = abundantNumbers
	// sum all numbers to check numbers until upperLimit
	for key, _ := range abundantNumbers {
		for copy, _ := range copyAbundantNumbers {
			sumOfAllAbundantNumbers[key+copy] = struct{}{}
		}
	}
	// check all numbers until the upperLimit to determine if they cannot be the sum
	// of two abundant numbers
	var sumOfNotAbundantNumbers uint64
	for i := uint64(1); i <= upperLimit; i++ {
		_, ok := sumOfAllAbundantNumbers[i]
		if !ok {
			sumOfNotAbundantNumbers += i
		}
	}
	fmt.Println(sumOfNotAbundantNumbers)
}

// isAbundantNumber will return true if the given number n is an abundant number
// an abundant number is defined as a number whose sum of it's divisors are greater
// then the number itself ex. 12: divisors: 1, 2, 3, 4, 6 sum: 16 sum > 12
func isAbundantNumber(n uint64) bool {
	var divisors []uint64
	// divisor is never great than half of n & int rounds decimal for odd numbers
	half := n / 2
	// one is always divisible from n, it gets added to sum below
	for i := uint64(2); i <= half; i++ {
		// check for divisors
		if isDivisor(n, i) {
			divisors = append(divisors, i)
		}
	}
	var sum uint64
	for _, divisor := range divisors {
		sum += divisor
	}
	// fmt.Println(sum + 1)
	return (sum + 1) > n
}

// isDivisor will return true if n is cleanly divisible by divide
func isDivisor(n, divide uint64) bool {
	return math.Mod(float64(n), float64(divide)) == 0
}
